from django.contrib import admin
from django.urls import path, include
from django.views import static

from bug import views
from bugstracker import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('hello/<msg3>', views.hello),
    # path('hi/', views.hi),
    path('create_bug/', views.create_bug),
    path('tinymce/', include('tinymce.urls')),
    path('', views.home),
]

if settings.DEBUG:
    urlpatterns.append(path('uploads/<path>', static.serve, {'document_root': settings.MEDIA_ROOT}, name='uploads'))

    urlpatterns.append(path(r'^uploads(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT}),)
