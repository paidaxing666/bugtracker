"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/8/20 10:58
@Author  : 派大星
@Site    : 
@File    : to_db.py
@Software: PyCharm
@desc:
"""
import pymysql
from pymysql.cursors import DictCursor

"""
MySQL封装成一个类，并实现以下方法
1、连接某个MySQL某个db，db不存在时支持自动创建并选择
2、新建并选择数据库、删除库
3、查询所有表名
4、新建表、删除表、插入数据、修改表、查询表数据数量

说明：可以使用一个类，也可以增加一个Table类
"""


def get_db_connect(host, port, user, password):
    """连接数据库，并返回链接对象"""
    conn = pymysql.connect(host=host, port=port, user=user, password=password, autocommit=True)
    return conn


class ToMySql:
    """操作MySQL"""

    def __init__(self, *, host, port, user, password):
        self.conn = get_db_connect(host, port, user, password)

    def to_db(self, db):
        """创建并选择数据库"""
        cursor = self.conn.cursor()
        cursor.execute(f'CREATE DATABASE IF NOT EXISTS {db}')
        self.conn.select_db(db)
        p_msg = f'新建并选择数据库-{db}' if cursor.rowcount else f'新建并选择数据库-{db}-失败'
        print(p_msg)
        return self.conn

    def del_db(self, db):
        cursor = self.conn.cursor()

        cursor.execute(f'DROP DATABASE IF EXISTS {db}')
        print(f'删除数据库-{db}-成功')
        return self.conn

    def close_db(self):
        """关闭连接"""
        self.conn.close()


class ToMySqlTable(ToMySql):
    """操作指定数据库中的表"""

    def __init__(self, *, host, port, user, password, db):
        super().__init__(host=host, port=port, user=user, password=password)
        # self.cursor = self.to_db(db).cursor(cursor=DictCursor)
        self.cursor = self.to_db(db).cursor()

    def execute(self, sql):
        """执行SQL"""
        try:
            self.cursor.execute(sql)
            sql_lower = sql.lower()
            if ('insert' in sql_lower or 'delete' in sql_lower or 'update' in sql_lower
                    or 'create' in sql_lower or 'drop' in sql_lower):
                result = f'Affected rows: {self.cursor.rowcount}'
            else:
                result = self.cursor.fetchall()
            return result
        except Exception as error:
            self.conn.rollback()
            print(f'在数据库中执行{sql}失败，返回信息{error}')

    def close_db(self):
        """关闭连接"""
        self.cursor.close()
        self.conn.close()


if __name__ == '__main__':
    cursor1 = ToMySqlTable(host='127.0.0.1', port=3306, user='root', password='playgood666', db='bugstracker')
    bug = cursor1.execute("DROP TABLE users4")
    print(bug)
    cursor1.close_db()
