from django.contrib.auth.models import User
from django.db import models
from tinymce.models import HTMLField


class ModelBase(models.Model):
    create_user = models.ForeignKey(User, verbose_name='创建人', related_name='created_%(class)s', null=True,
                                    on_delete=models.SET_NULL)
    update_user = models.ForeignKey(User, verbose_name='修改人', related_name='updated_%(class)s', null=True,
                                    on_delete=models.SET_NULL)

    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='修改时间', auto_now=True)

    class Meta:
        abstract = True  # 抽象模型，只用于继承，不创建


class Project(ModelBase):
    """项目"""
    name = models.CharField(verbose_name='项目名', max_length=128)
    icon = models.ImageField(verbose_name='项目图标', null=True, blank=True)
    description = models.TextField(verbose_name='项目描述', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '项目'
        verbose_name_plural = '项目列表'


class Plan(ModelBase):
    """计划"""
    name = models.CharField(verbose_name='计划名称', max_length=128)
    description = models.TextField(verbose_name='计划描述', null=True, blank=True)
    project = models.ForeignKey(Project, verbose_name='所属项目', on_delete=models.CASCADE)

    start_time = models.DateTimeField(verbose_name='开始时间')
    end_time = models.DateTimeField(verbose_name='结束时间')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '计划'
        verbose_name_plural = '计划列表'


class Story(ModelBase):
    """需求"""
    SOURCE_CHOICES = [
        ('customer', '客户'),
        ('developer', '开发'),
        ('tester', '测试'),
        ('other', '其他')
    ]

    name = models.CharField(verbose_name='需求名称', max_length=128)
    description = models.TextField(verbose_name='需求描述', null=True, blank=True)
    plan = models.ForeignKey(Plan, verbose_name='所属计划', null=True, blank=True, on_delete=models.CASCADE)
    source = models.CharField(verbose_name='需求来源', max_length=32, choices=SOURCE_CHOICES, default='customer')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '需求'
        verbose_name_plural = '需求列表'


class Bug(ModelBase):
    """缺陷"""
    # 严重等级
    SEVERITY_CHOICE = [
        (0, '阻塞'),
        (1, '严重'),
        (2, '一般'),
        (3, '轻微')
    ]

    # 状态
    STATUS_CHOICES = [
        ('new', '新建'),
        ('processing', '处理中'),
        ('solved', '已解决'),
        ('rejected', '拒绝'),
        ('closed', '已关闭')
    ]

    title = models.CharField(verbose_name='标题', max_length=128, help_text='缺陷标题')
    description = HTMLField(verbose_name='缺陷描述', null=True, blank=True)

    project = models.ForeignKey(Project, verbose_name='所属项目', null=True, on_delete=models.CASCADE)
    stories = models.ManyToManyField(Story, verbose_name='关联需求', null=True, blank=True)

    severity = models.SmallIntegerField(verbose_name='严重等级', choices=SEVERITY_CHOICE, default=2)
    status = models.CharField(verbose_name='状态', max_length=32, choices=STATUS_CHOICES, default='new')
    solution = HTMLField(verbose_name='解决方案', null=True, blank=True)

    assignee = models.ForeignKey(User, verbose_name='指定给', related_name='assignee_bugs', null=True,
                                 on_delete=models.SET_NULL)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '缺陷'
        verbose_name_plural = '缺陷列表'


class Attachment(ModelBase):
    """附件"""
    file = models.FileField(verbose_name='附件', upload_to='uploads')
    bug = models.ForeignKey(Bug, verbose_name='所属缺陷', related_name='attachments', on_delete=models.CASCADE)
    remark = models.CharField(verbose_name='附件说明', max_length=128, null=True, blank=True)

    def __str__(self):
        return self.file.name

    class Meta:
        verbose_name = '附件'
        verbose_name_plural = '附件列表'
