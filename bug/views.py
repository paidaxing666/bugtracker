import json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Bug


# Create your views here.

# @csrf_exempt
# def hello(request, msg3):
#     # msg = request.GET.get('msg')
#     # msg_1 = request.POST.get('msg_1')
#     # data = json.loads(request.body)
#     # msg_2 = data.get('msg_2')
#     return HttpResponse(F'hello {msg3}')
#
#
# def hi(request):
#     data = {"message": "hello django"}
#     return render(request, 'hi.html', data)

@csrf_exempt
def create_bug(request):
    title = request.POST.get('title')
    description = request.POST.get('description')
    severity = request.POST.get('severity')
    status = request.POST.get('status')

    bug = Bug(title=title, description=description)
    bug.save()
    return JsonResponse({'code': 0, 'msg': 'Success'}, status=201)


def home(request):
    return HttpResponseRedirect('/admin/')
