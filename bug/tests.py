from django.test import TestCase
import os
import django


# 设置Django项目配置的环境变量
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bugtracker.settings')
django.setup()
