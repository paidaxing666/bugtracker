from django.contrib import admin
from django.utils.html import format_html
from bug.models import Bug, Project, Story, Attachment, Plan

admin.site.site_header = "gbz自定义管理后台"
admin.site.site_title = "自定义管理后台"
admin.site.index_title = "欢迎━(*｀∀´*)ノ亻!"


class AdminModelBase(admin.ModelAdmin):
    exclude = ['create_user', 'update_user']  # 排除字段

    def save_bug(self, request, obj, form, change):
        if not obj.pk:
            obj.create_user = request.user
        obj.update_user = request.user
        super().save_model(request, obj, form, change)


class AttachmentInline(admin.TabularInline):
    model = Attachment
    extra = 1


class StoryInline(admin.TabularInline):
    """需求"""
    model = Story
    extra = 0
    list_display = ['id', 'name', 'description', 'source', 'create_user', 'create_time', 'update_time']


@admin.register(Project)
class ProjectAdmin(AdminModelBase):
    """项目"""
    admin_order = 1
    list_display = ['id', 'name', 'icon_image', 'description', 'create_user', 'create_time', 'update_time']
    list_display_links = ['name']

    fields = ['name', 'icon', 'icon_image_big', 'description']
    readonly_fields = ['icon_image_big']

    @admin.display(description='项目图标')
    def icon_image(self, obj):
        if obj.icon:
            return format_html(f'<img src="/uploads/{obj.icon}" width=32 />')
        else:
            return '无'

    @admin.display(description='项目预览')
    def icon_image_big(self, obj):
        if obj.icon:
            return format_html(f'<img src="/uploads/{obj.icon}" width=100 />')
        else:
            return '无'


@admin.register(Plan)
class PlanAdmin(AdminModelBase):
    """计划"""
    admin_order = 2
    list_display = ['id', 'name', 'description', 'project', 'start_time', 'end_time']
    list_display_links = ['name']
    inlines = [StoryInline]


@admin.register(Story)
class StoryAdmin(AdminModelBase):
    """需求"""
    admin_order = 3
    list_display = ['id', 'name', 'description', 'plan', 'create_user', 'create_time']
    list_display_links = ['name']


@admin.register(Bug)
class BugAdmin(AdminModelBase):
    # 列表页相关配置
    admin_order = 4
    list_display = ['id', 'title', 'project', 'colored_severity', 'status', 'assignee', 'create_user', 'create_time',
                    'update_time',
                    'operations']
    list_filter = ['project', 'severity', 'status']
    list_editable = ['status', 'assignee']
    list_display_links = ['id', 'title']
    search_fields = ['title']

    actions = ['copy_bug']

    # 编辑页相关配置
    fields = ['title', ('project', 'severity', 'assignee'), 'description', 'stories']
    filter_horizontal = ['stories']

    inlines = [AttachmentInline]  # 内联

    @admin.display(description='创建用户', ordering='create_user')
    def create_user_real_name(self, obj):
        if obj and obj.create_user:
            create_user = obj.create_user
            real_name = f'{create_user.last_name} {create_user.first_name}'
            return real_name

    @admin.display(description='严重等级')
    def colored_severity(self, obj):
        color_map = {0: 'purple', 1: 'red', 2: 'orange', 3: 'blue'}
        color = color_map.get(obj.severity, 'black')
        return format_html(f'<span style="color: {color}">{obj.get_severity_display()}</span>')

    @admin.display(description='操作')
    def operations(self, obj):
        html = '<div> <a href="#">查看</a> <a href="#">删除</a>'

        return format_html(html)

    @admin.action(description='复制')
    def copy_bug(self, request, queryset):
        for bug in queryset:
            new_bug = Bug(title=bug.title + '复制', description=bug.description, severity=bug.severity,
                          status=bug.status,
                          assignee=bug.assignee, create_user=bug.create_user, update_user=bug.update_user)
            new_bug.save()
