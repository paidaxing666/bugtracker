from django.apps import AppConfig


class BugConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bug'
    verbose_name = '缺陷管理'
